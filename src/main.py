import csv
import sys
import numpy as np
import matplotlib.pyplot as plt
from statistics import mean

#13/31, 19/43, 22/37, 17/39 vlevo blíž k motoru (13 zubů na motoru pohání 31, na něm přímo 19 zubů v kontaktu se 43 atd. enkodér na motoru (13. zubé kolo))

def dataResample(diff, period):
  num_samples = len(diff)//period
  if ((len(diff)/period) > num_samples):
    num_samples += 1

  array_size = []
  for i in range(0, num_samples):
    array_size.append((i+len(diff))//num_samples)

  posLast = 0
  for i in range(0, num_samples):
    diffMean = mean(diff[posLast:(posLast + array_size[i])])
    for j in range(posLast, posLast + array_size[i]):
      diff[j] = diff[j] * 1/diffMean
    posLast = posLast + array_size[i]

  return diff

def dataFFT(csvreader, resampleSize):
  rows = []
  for row in csvreader:
    rows.append(int(row[0]))

  file.close()

  diff = []
  x = []
  for i in range(1, len(rows)):
    diff.append(rows[i] - rows[i-1])
    x.append(i)

  diffMean = mean(diff)
  for i in range(0, len(diff)):
    if ((diff[i] > 1.2*diffMean) or (diff[i] < 0.8*diffMean)):
      diff[i] = diffMean

  diff = dataResample(diff, resampleSize)

  diffFFT = abs(np.fft.fft(diff))
  diffFFT[0] = 0

  diffFFT = diffFFT[:int(len(rows)/2)]

  x_new = []
  for i in range(0, len(diffFFT)):
    x_new.append(i)
    if diffFFT[i] > 5:
        None
    else:
       diffFFT[i] = 0
       #None

  return x_new, diffFFT

def generateSin(frequency):

  end_time = 1
  sample_rate = 40000

  time = np.arange(0, end_time, 1/sample_rate)
  amplitude = 1
  sinewave = amplitude * np.sin(2 * np.pi * frequency * time)

  sinewaveFFT = abs(np.fft.fft(sinewave))
  sinewaveFFT[0] = 0
  sinewaveFFT = sinewaveFFT[:int(len(time)/2)]

  x_new = []
  for i in range(0, len(sinewaveFFT)):
    x_new.append(i)
    if sinewaveFFT[i] > 300:
      sinewaveFFT[i] = 300

  return x_new, sinewaveFFT

if __name__ == '__main__':

  if (len(sys.argv) < 5):
    print("ERROR: Incorrect input parameters")
    print("python main.py [fault data] [nofault data] [resample size] [RPM]")
    quit()

  file = open(str(sys.argv[1]))
  file2 = open(str(sys.argv[2]))
  resampleSize = int(sys.argv[3])
  rpm = int(sys.argv[4])
  csvreader = csv.reader(file)
  csvreader2 = csv.reader(file2)

  x1, data1 = dataFFT(csvreader, resampleSize)
  x2, data2 = dataFFT(csvreader2, resampleSize)

  f = (400*(rpm//60))

  x3, data3 = generateSin(f)

  plt.rcParams["figure.figsize"] = [7.50, 3.50]
  plt.rcParams["figure.autolayout"] = True
  plt.rcParams['font.size'] = 26

  plt.title("Order spectrum")
  #plt.plot(x1, data1, color="blue",)
  plt.plot(x1, data1, color="red",  alpha=0.6)
  plt.plot(x2, data2, color="blue",  alpha=0.6)
  #plt.plot(x3[0:3000], data3[0:3000], color="green",  alpha=0.6)
  plt.legend(["fault", "nofault"])
  plt.xlabel("Frequency [Hz]")
  plt.ylabel("Amplitude [-]")

  plt.show()