Usage:
  python src/main.py [fault data] [nofault data] [resample size] [RPM]

  fault data = cvs file with fault encoder data
  nofault data = cvs file with correct encoder data
  resample size = number of samples used for resampling
  RPM = motor's rpm (required only for reference sine frequency)